package br.com.mastertech.imersivo.kafkaproducer.service;


import br.com.mastertech.imersivo.kafkaproducer.model.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {

    @Autowired//       (tipo chave, tipo valor)
    private KafkaTemplate<String, Acesso> sender;

    public void enviarAcesso(Acesso acesso) {
        //         (topico, chave, valor)
        sender.send("leandro", "1", acesso);
    }
}