package br.com.mastertech.imersivo.kafkaproducer.controller;

import br.com.mastertech.imersivo.kafkaproducer.model.Acesso;
import br.com.mastertech.imersivo.kafkaproducer.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@RestController
public class ProducerController {

    @Autowired
    private KafkaProducerService producerService;

    @PostMapping("/acesso")
    private void registraAcesso(@RequestBody Acesso acesso) {
        Random random = new Random();
        acesso.setAcesso(random.nextBoolean());
        acesso.setHorario(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy-hh:mm:ss")));

        producerService.enviarAcesso(acesso);
    }
}