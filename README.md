# Exercício Kafka

Crie um microsserviço que quando acessado /acesso, verifique se o cliente tem acesso a porta (por uma boolean gerada aleatoriamente) e grave o log dessa ação no seu tópico do Kafka.

Crie um microsserviço reativo que leia os logs de acesso e os grave em arquivos .csv em disco.
