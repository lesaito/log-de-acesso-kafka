package br.com.mastertech.imersivo.kafkaconsumer.controller;

import br.com.mastertech.imersivo.kafkaproducer.model.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class KafkaConsumerController {

    private static final String NOME_ARQUIVO = "acesso.csv";

    @KafkaListener(topics = "leandro", groupId = "leandro")
    public void recebeAcesso(@Payload Acesso acesso) {
        System.out.println("Recebi um acesso do: " + acesso.getCliente());
        System.out.println("Porta: " + acesso.getPorta());
        System.out.println("Horario: " + acesso.getHorario());
        System.out.println("Possui acesso: " + acesso.isAcesso());
        System.out.println("");

        escreveArquivoAcessos(NOME_ARQUIVO, acesso);
    }


    private void escreveArquivoAcessos(String arquivo, Acesso acesso) {
        try {
            String registro = String.format("%s,%s,%s,%s\n", acesso.getCliente(), acesso.getPorta(), acesso.getHorario(), acesso.isAcesso());
            Files.write(Paths.get(arquivo), registro.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
